module Main exposing (Model, Msg(..), homeEmuSource, init, main, update, view)

import Browser
import Html exposing (Html, div, h1, img, text)
import Html.Attributes exposing (src)



---- Inline Source View ----
-- eventually we will want a standalone emu file
-- to be used, but usually that involves the elm
-- app making an unnecessary HTTP request
-- just to grab a static asset. We can create
-- a pre-build step that copies a valid
-- .emu file into a placeholder
-- variable tho, then it'll all be compiled together


homeEmuSource =
    """
View your infrastructure here!
"""



---- MODEL ----


type alias Model =
    {}


init : ( Model, Cmd Msg )
init =
    ( {}, Cmd.none )



---- UPDATE ----


type Msg
    = NoOp


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    ( model, Cmd.none )



---- VIEW ----


view : Model -> Html Msg
view model =
    div []
        [ img [ src "/logo.svg" ] []
        , h1 [] [ text "Your Elm App is working!" ]
        ]



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = always Sub.none
        }
