# Overview

An app to manage a private cloud Simple UI to quickly
execute terraform or kubectl commands

## Goals

- User sees their infrastructure
- User can interact with their infrastructure in the browser
- User sees high level constructs
  - compose yaml file
  - inventory
  - terraform config
  - kubernetes pods (health?)

The idea is to provide high level overview of infrastructure
at a glance, and show important things, with some interactive
click-able, drill-downs where necessary. Perhaps we don't
even see the pods health in the dashboard, but maybe
just the cluster's, and clicking into a service or something
shows the pods.

## Front End

Expose the terraform files or docker files, any and all yaml
should be editable and saveable.

Provide simple UI over the files for at-a-glance viewing.

Done in Elm, should parse any and all supported editeable files
to render a view

## Back End

Expose the data store and connect to any service(s) required for
the front end to do its job well. This will include exposing
an API to execute any commands or changes the user wants
to run on their infrastructure, expose an API to fetch
the terraform inventory and any other yaml files.

The backend will be a simple Suave F# API to get going quickly.

We may need a queuing system to allow another long running process
to execute shell commands or edit files on a remote server
or some cloud storage provider. That kinda depends on how the
project will work with persisting the infrastructure as code
part of the infrastructure.

We could just work with git repositories, and point to docs
to setup CI/CD on those repos so the pipeline auto-deploys
the newly committed configuration.

# Dev

First thing's first, make a scaffolded dummy project that all
works together and can be dockerized.

- Frontend with static text
- Backend can return dummy JSON
- Frontend calls backend, renders the JSON

## Ideas

Elm markup seems pretty cool! I've loved using elm-ui in the past,
but really do not like writing out huge chunks of nodes
or elements, so the markup should feel closer to the browser
but still allow the strong typing of elm.

Suave is definitely a really easy way to get endpoints up
and running, but I'm not so sure it will be the best idea
to have a kestrel worker thread be handling all the remote
services whether that's just a git repo integration,
or a more sophisticated queued job-running system (like 
executing docker-compose up for example, which might take
a while). We may defer that and focus on git integrations for now.

