# Infrastructure As Code UI

_This is a brand new project!_ As such, there isn't much here.

## Goals

1. Provide a UI to common yaml files
2. The UI should be a good, useful at-a-glance view
3. Provide minimal management actions
4. Expose and and all files
5. Eventually, be able to execute commands on your fleet from the comfort of your browser

## What I need

Just ideas and thoughts for now. Feel free to fork if you want
to write some code. I likely won't accept PRs into the mainline
master branch as I'm working on it, and I make no
promises that this won't destroy your fleet. Consider yourself
warned!

## What this is not

An enterprise tool. A polished product. A way to make money.

This is a fun tool!
